module gitlab.com/loranna/appout

go 1.13

require (
	github.com/gorilla/websocket v1.4.1
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/cobra v0.0.7
	gitlab.com/loranna/loranna v0.0.7
	gitlab.com/loriot/api v0.2.27
)
