# Appout

Appout is a CLI and connects to LORIOT websocket application output and logs all messages coming from LORIOT

## Available commands

See [available_commands.md](available_commands.md)

## bash completion usage

- make sure you have `bash-completion` package
- copy `bash.sh` and rename it to `appout` to bash-completion directory
- make sure functionality is enabled in `/etc/bash.bashrc` or `~/.bashrc`

## Config file

Config file is a properly formatted yaml file.

### Example

```yaml
loriotapiconfig:
  loriotserverurl: # loriot server url
  loriotapikey: # loriot API key
```
