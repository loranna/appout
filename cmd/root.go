package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	conf "gitlab.com/loranna/loranna/config"
)

type AppoutConfig struct {
	conf.LoriotApiConfig
}

var config AppoutConfig

var rootCmd = &cobra.Command{
	Use:   "appout",
	Short: "monitors a loriot app output",
	Long: `appout connects to LORIOT application websocket output 
and logs the data coming out of it`,
	SilenceErrors: false,
	SilenceUsage:  true,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		logrus.Fatal(err)
	}
}
