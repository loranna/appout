# CHANGELOG

## 0.2.18

- connects to LORIOT websocket output
- parses `rx` messages
- parses `gw` messages
- prints parsed messages
