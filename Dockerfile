# build stage
FROM golang:1.13 AS build
ADD go.mod /app/
WORKDIR /app

ARG GOPROXY
ARG MAJOR
ARG MINOR
ARG COMMITCOUNT
ARG GONOSUMDB

ENV CGO_ENABLED=0
RUN go mod download
ADD . /app
RUN ./scripts/build.sh

# final stage
FROM ubuntu:18.04
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install ca-certificates -y --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*
COPY --from=build /app/appout /appout
COPY --from=registry.gitlab.com/loranna/bootscripts:0.2.33 /bootscripts /bootscripts
ENTRYPOINT ["/appout"]
CMD [ "version" ]