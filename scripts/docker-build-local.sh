#!/bin/bash

TAG=$(git tag -l --sort=-v:refname | head -n 1 | cut -c 2-)
IFS='.' read -ra vers <<< "$TAG"
MAJOR="${vers[0]}"
MINOR="${vers[1]}"
COMMITCOUNT="${vers[2]}"

./scripts/build.sh

docker build -f Dockerfile_local \
  -t registry.gitlab.com/loranna/appout:latest .

rm appout
