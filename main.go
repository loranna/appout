package main

import (
	"os"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/loranna/appout/cmd"
)

func main() {
	delay := os.Getenv("START_DELAY")
	if len(delay) > 0 {
		delaysec, err := strconv.Atoi(delay)
		if err != nil {
			delaysec = 0
		}
		time.Sleep(time.Duration(delaysec) * time.Second)
	}
	logrus.SetLevel(logrus.TraceLevel)
	cmd.Execute()
}
