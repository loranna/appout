package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	conf "gitlab.com/loranna/loranna/config"
	"gitlab.com/loriot/api/core"
)

func init() {
	rootCmd.AddCommand(startCmd)
}

type Cmd struct {
	Cmd string `json:"cmd"`
}

type Rx struct {
	Cmd
	Seqno   int     `json:"seqno"`
	EUI     string  `json:"EUI"`
	Ts      int64   `json:"ts"`
	Fcnt    int     `json:"fcnt"`
	Port    int     `json:"port"`
	Freq    int     `json:"freq"`
	Rssi    int     `json:"rssi"`
	Snr     float64 `json:"snr"`
	Dr      string  `json:"dr"`
	Ack     bool    `json:"ack"`
	Bat     int     `json:"bat"`
	Offline bool    `json:"offline"`
	Data    string  `json:"data"`
}

type Gw struct {
	Cmd
	Seqno int       `json:"seqno"`
	EUI   string    `json:"EUI"`
	Ts    int64     `json:"ts"`
	Fcnt  int       `json:"fcnt"`
	Port  int       `json:"port"`
	Freq  int       `json:"freq"`
	Dr    string    `json:"dr"`
	Ack   bool      `json:"ack"`
	Gws   []Gateway `json:"gws"`
	Bat   int       `json:"bat"`
	Data  string    `json:"data"`
}
type Gateway struct {
	Rssi  int       `json:"rssi"`
	Snr   float64   `json:"snr"`
	Ts    int64     `json:"ts"`
	Tmms  int       `json:"tmms"`
	Time  time.Time `json:"time"`
	Gweui string    `json:"gweui"`
	Lat   float64   `json:"lat"`
	Lon   float64   `json:"lon"`
}

type TransmitDelivered struct {
	Cmd          string  `json:"cmd"`
	EUI          string  `json:"EUI"`
	Seqdn        int     `json:"seqdn"`
	Seqq         int     `json:"seqq"`
	Ts           int64   `json:"ts"`
	Gweui        string  `json:"gweui"`
	Freq         int     `json:"freq"`
	Sf           int     `json:"sf"`
	Toa          float64 `json:"toa"`
	MsgType      string  `json:"msgType"`
	Time         int64   `json:"time"`
	AckRequested bool    `json:"ackRequested"`
}

var conntoenv *websocket.Conn

var connsmutex sync.Mutex
var conns = make(map[string]*websocket.Conn, 0)

var errorChannel = make(chan error)

var startCmd = &cobra.Command{
	Use:     "start",
	Short:   "starts app out",
	Example: `appout start --loriotserverurl eu1.loriot.io --loriotapikey <API_KEY>`,
	RunE: func(cmd *cobra.Command, args []string) error {
		dir, err := os.Getwd()
		if err != nil {
			return err
		}
		filename := path.Join(dir, "config.yml")
		err = conf.LoadYamlFromFile(filename, &config)
		if err != nil {
			return err
		}
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)

		url := config.LoriotServerURL
		apikey := config.LoriotAPIKey
		connsmutex = sync.Mutex{}
		deviceslistconn, _, err := websocket.DefaultDialer.Dial("ws://localhost:8081/device/list", nil)
		if err != nil {
			return err
		}
		defer func() {
			deviceslistconn.WriteMessage(websocket.CloseMessage, nil)
			deviceslistconn.Close()
		}()

		go func() {
			for {
				_, message, err := deviceslistconn.ReadMessage()
				if err != nil {
					errorChannel <- fmt.Errorf("deviceslistconn.ReadMessage failed: %w", err)
					return
				}
				var deveui string
				err = json.Unmarshal(message, &deveui)
				if err != nil {
					errorChannel <- fmt.Errorf("json.Unmarshal failed: %w", err)
					return
				}
				logrus.Tracef("new deveui received: %s", deveui)
				connsmutex.Lock()
				_, ok := conns[deveui]
				if ok {
				} else {
					token, err := findtoken(url, apikey, deveui)
					if err != nil {
						errorChannel <- fmt.Errorf("findtoken failed: %w", err)
						return
					}
					conn, _, err := websocket.DefaultDialer.Dial(fmt.Sprintf("wss://%s/app?token=%s", url, token), nil)
					if err != nil {
						errorChannel <- fmt.Errorf("DefaultDialer.Dial failed: %w", err)
						return
					}
					conns[deveui] = conn
					go handlemessages(conn)
				}
				connsmutex.Unlock()
			}
		}()
		select {
		case <-c:
			return nil
		case err := <-errorChannel:
			for _, conn := range conns {
				conn.WriteMessage(websocket.CloseMessage, nil)
				conn.Close()
			}
			return err
		}
	},
}

func findtoken(url, apikey, deveui string) (string, error) {
	loriot := core.NewClient(url, apikey)
	apps, err := loriot.ApplicationList()
	if err != nil {
		return "", err
	}
	for _, app := range apps {
		devices, err := loriot.DevicesList(fmt.Sprintf("%08X", app.ID))
		if err != nil {
			return "", err
		}
		for _, device := range devices {
			if strings.ToUpper(device.ID) == strings.ToUpper(deveui) {
				tokens, err := loriot.AccessTokens(fmt.Sprintf("%08X", app.ID))
				if err != nil {
					return "", err
				}
				return tokens[0], nil
			}
		}
	}
	return "", fmt.Errorf("deveui %s is not registered on %s", deveui, url)
}

func handlemessages(conn *websocket.Conn) {
	for {
		_, message, err := conn.ReadMessage()
		cmd := &Cmd{}
		err = json.Unmarshal(message, cmd)
		if err != nil {
			errorChannel <- fmt.Errorf("json.Unmarshal failed: %w", err)
			return
		}
		switch cmd.Cmd {
		case "rx":
			rx := &Rx{}
			err := json.Unmarshal(message, rx)
			if err != nil {
				errorChannel <- fmt.Errorf("json.Unmarshal1 failed: %w", err)
				return
			}
			logrus.Infof("%+v", rx)
		case "gw":
			gw := &Gw{}
			err := json.Unmarshal(message, gw)
			if err != nil {
				errorChannel <- fmt.Errorf("json.Unmarshal2 failed: %w", err)
				return
			}
			logrus.Infof("%+v", gw)
		case "txd":
			txd := &TransmitDelivered{}
			err := json.Unmarshal(message, txd)
			if err != nil {
				errorChannel <- fmt.Errorf("json.Unmarshal3 failed: %w", err)
				return
			}
			logrus.Infof("%+v", txd)
		default:
			logrus.Warnf("unknown cmd %s in message %s", cmd.Cmd, string(message))
		}
	}
}
